const botao = document.querySelector(".botao")

botao.addEventListener('click', () => {
    var texto = document.querySelector(".texto").value 
    var enviado = document.createElement('div')
    var mensagens = document.querySelector('.mensagens')
    enviado.innerHTML = `<p>${texto}</p>
    <div>
        <button class='botaoEditar' onclick='editar1(this)'>Editar</button>
        <button class='botaoExcluir' onclick='excluir(this)'>Excluir</button>
    </div>
    <div hidden>
        <input type='text'> 
        <button onclick='editar2(this)'>Editar</button>
    </div>`


mensagens.appendChild(enviado)

}) 

function excluir(elemento) {
    elemento.parentElement.parentElement.remove()
}

function editar1(elemento) {
    elemento.parentElement.toggleAttribute('hidden')
    elemento.parentElement.parentElement.children[2].toggleAttribute('hidden')
}

function editar2(elemento) {
    console.log(elemento.parentElement.firstElementChild)
    elemento.parentElement.parentElement.firstChild.innerText = elemento.parentElement.firstElementChild.value
    elemento.parentElement.toggleAttribute('hidden')
    elemento.parentElement.parentElement.children[1].toggleAttribute('hidden')

}

